#ifndef STUDENT_H
#define STUDENT_H

#include <string>

class Student {
    private:
        std::string name;
    public:
        // constructor
        Student(const std::string& name);

        std::string getName() const;
};

#endif 

