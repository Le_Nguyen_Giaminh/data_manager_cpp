#include <iostream>
#include <vector>
#include <string>
#include "subject.h"
#include "teacher.h"
#include "student.h"

void runApplication() {
    std::vector<Teacher> teachers;
    std::vector<Student> students;
    std::vector<Subject> subjects;

    int choice;

    std::cout << "----Welcome to ABC School's data manager----\n";

    do {
        std::cout << "Menu:\n";
        std::cout << "1. Add Teacher\n";
        std::cout << "2. Add Student\n";
        std::cout << "3. Add Subject\n";
        std::cout << "4. Display Subject Details\n";
        std::cout << "5. Exit\n";
        std::cout << "Enter your choice: ";
        std::cin >> choice;

        switch (choice) {
            case 1: {
                std::string name;
                std::cout << "Enter teacher name: ";
                std::cin >> name;
                Teacher teacher(name);
                teachers.push_back(teacher);
                break;
            }
            case 2: {
                std::string name;
                std::cout << "Enter student name: ";
                std::cin >> name;
                Student student(name);
                students.push_back(student);
                break;
            }
            case 3: {
                std::string name;
                std::cout << "Enter subject name: ";
                std::cin >> name;
                Subject subject(name);
                subjects.push_back(subject);

                int teacherCount;
                std::cout << "Enter the number of teachers for this subject: ";
                std::cin >> teacherCount;
                for (int i = 0; i < teacherCount; ++i) {
                    int teacherIndex;
                    std::cout << "Enter teacher index (starting from 0): ";
                    std::cin >> teacherIndex;
                    subject.addTeacher(teachers[teacherIndex]);
                }

                int studentCount;
                std::cout << "Enter the number of students for this subject: ";
                std::cin >> studentCount;
                for (int i = 0; i < studentCount; ++i) {
                    int studentIndex;
                    std::cout << "Enter student index (starting from 0): ";
                    std::cin >> studentIndex;
                    subject.addStudent(students[studentIndex]);
                }

                break;
            }
            case 4: {
                if (subjects.empty()) {
                    std::cout << "No subjects available.\n";
                } else {
                    int subjectIndex;
                    std::cout << "Enter subject index (starting from 0): ";
                    std::cin >> subjectIndex;

                    if (subjectIndex < 0 || subjectIndex >= subjects.size()) {
                        std::cout << "Invalid subject index.\n";
                    } else {
                        subjects[subjectIndex].displayDetails();
                    }
                }
                break;
            }
            case 5: {
                std::cout << "Exiting the application.\n";
                break;
            }
            default:
                std::cout << "Invalid choice. Please try again.\n";
                break;
        }

        std::cout << std::endl;
    } while (choice != 5);
}

int main() {
    runApplication();
    return 0;
}
