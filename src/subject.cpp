#include "subject.h"
#include <iostream>

Subject::Subject(const std::string& name) : name(name) {}

std::string Subject::getName() const {
    return name;
}

void Subject::addTeacher(const Teacher& teacher) {
    teachers.push_back(teacher);
}

void Subject::addStudent(const Student& student) {
    students.push_back(student);
}

void Subject::displayDetails() const {
    std::cout << "Subject: " << name << std::endl;
    std::cout << "Teachers: ";
    for (const auto& teacher : teachers) {
        std::cout << teacher.getName() << ", ";
    }
    std::cout << std::endl;
    std::cout << "Students: ";
    for (const auto& student : students) {
        std::cout << student.getName() << ", ";
    }
    std::cout << std::endl;
}
