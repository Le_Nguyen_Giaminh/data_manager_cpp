#ifndef TEACHER_H
#define TEACHER_H

#include <string>

class Teacher {
    private:
        std::string name;

    public:
        // constructor
        Teacher(const std::string& name);

        std::string getName() const;
};

#endif 
