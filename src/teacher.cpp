#include "teacher.h"

Teacher::Teacher(const std::string& name) : name(name) {}

std::string Teacher::getName() const {
    return name;
}
