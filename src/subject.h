#ifndef SUBJECT_H
#define SUBJECT_H

#include <string>
#include <vector>
#include "teacher.h"
#include "student.h"

class Subject {
    private:
        std::string name;
        std::vector<Teacher> teachers;
        std::vector<Student> students;

    public:
        // constructor
        Subject(const std::string& name);

        std::string getName() const;
        void addTeacher(const Teacher& teacher);
        void addStudent(const Student& student);
        void displayDetails() const;
};

#endif  
